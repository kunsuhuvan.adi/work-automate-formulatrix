from selenium import webdriver
import time

driver = webdriver.Chrome(executable_path="C:\\Users\\HP\\Downloads\\chromedriver.exe")
print(type(driver))
driver.maximize_window()
driver.get('https://www.demoblaze.com/')
time.sleep(3)


#signup
driver.find_element_by_id('signin2').click()
time.sleep(1)
username = 'User1'
password = '123456'
driver.find_element_by_id('sign-username').send_keys(username)
driver.find_element_by_id('sign-password').send_keys(password)
driver.find_element_by_xpath('//*[@id="signInModal"]/div/div/div[3]/button[2]').click()
driver.refresh()
if True:
    print('Successful Register / Pass')
else:
    print('Error')

#login
driver.find_element_by_id('login2').click()
time.sleep(5)
driver.find_element_by_id('loginusername').send_keys(username)
driver.find_element_by_id('loginpassword').send_keys(password)
driver.find_element_by_xpath('//*[@id="logInModal"]/div/div/div[3]/button[2]').click()
time.sleep(3)
if True:
    print('Login Success/ Pass')
time.sleep(3)

#choose item phone samsung galaxy s6
driver.find_element_by_link_text('Samsung galaxy s6').click()
time.sleep(5)
driver.find_element_by_css_selector('.btn.btn-success.btn-lg').click()
driver.refresh()
time.sleep(3)
driver.find_element_by_partial_link_text('Home').click()

#choose  item laptop macbook air
driver.find_element_by_xpath('/html/body/div[5]/div/div[1]/div/a[3]').click()
time.sleep(3)
driver.find_element_by_link_text('MacBook air').click()
time.sleep(5)
driver.find_element_by_link_text('Add to cart').click()
driver.refresh()
time.sleep(3)
driver.find_element_by_partial_link_text('Home').click()

#choose item monitor
driver.find_element_by_xpath('/html/body/div[5]/div/div[1]/div/a[4]').click()
time.sleep(3)
driver.find_element_by_link_text('ASUS Full HD').click()
time.sleep(3)
driver.find_element_by_partial_link_text('Add to ca').click()
driver.refresh()
time.sleep(3)
if True:
    print('Choose item to cart success / Pass')
else:
    print('Error')
time.sleep(3)

#delete cart
driver.find_element_by_id('cartur').click()
time.sleep(5)
driver.find_element_by_xpath('/html/body/div[6]/div/div[1]/div/table/tbody/tr[1]/td[4]/a').click()
time.sleep(3)
if True:
    print ('Delete Success / Pass')

#place order
driver.find_element_by_xpath('/html/body/div[6]/div/div[2]/button').click()
time.sleep(3)
driver.find_element_by_id('name').send_keys(username)
time.sleep(2)
driver.find_element_by_id('card').send_keys('BAC-150')
time.sleep(5)
driver.find_element_by_xpath('//*[@id="orderModal"]/div/div/div[3]/button[2]').click()
if True:
    print('Credit Card must be number, country, city, month and year of Credit Card must be filled / BUG')
else:
    print('Pass')
time.sleep(3)
driver.find_element_by_xpath('/html/body/div[10]/div[7]/div/button').click()
time.sleep(3)

#send message using contact feature
driver.find_element_by_link_text('Contact').click()
time.sleep(3)
driver.find_element_by_xpath('//*[@id="exampleModal"]/div/div/div[3]/button[2]').click()
time.sleep(5)
if True:
    print('Message can send without filling required field (email, contact name and message / BUG')
driver.refresh()
time.sleep(6)

#log out
driver.find_element_by_xpath('/html/body/nav/div[1]/ul/li[6]/a').click()
time.sleep(3)
if True:
    print('Logout success / Pass')
driver.quit()

